package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.Utility;

import java.text.ParseException;

public class Tests {
    @Test
    void toCamelCase() {
        Assertions.assertEquals("Male",new Utility().toCamelCase("male"));
    }

    @Test
    void dateFormatter() {
    }

    @Test
    void integerToString() {
    }
    @Test
    void add() {
    }

    @Test
    void update() {
    }

    @Test
    void read() {
    }

    @Test
    void delete() {
    }

    @Test
    void search() {
    }

    @Test
    void displayAll() {
    }

    @Test
    void displaySingle() {
    }

    @Test
    void issueBooks() {
    }
}
