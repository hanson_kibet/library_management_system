package Exceptions;

public class InvalidDateFormat extends Exception {
    public InvalidDateFormat() {
        super("Invalid date entered");
    }
}
